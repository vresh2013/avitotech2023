module avitotech

go 1.18

require (
	github.com/go-sql-driver/mysql v1.7.1
	github.com/golang/mock v1.6.0
	github.com/gorilla/mux v1.8.0
	github.com/sirupsen/logrus v1.9.3
	github.com/stretchr/testify v1.7.0
	gopkg.in/DATA-DOG/go-sqlmock.v1 v1.3.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
)
