# FROM golang:1.18

# COPY . /go/src/avitotech2023

# WORKDIR /go/src/avitotech2023/cmd/app

# RUN go build . && cp app /usr/local/bin

# EXPOSE 8080/tcp

# CMD ["app"]


# step 1
FROM golang:1.18 as build_step
COPY . /go/src/avitotech2023
WORKDIR /go/src/avitotech2023/cmd/app
RUN CGO_ENABLED=0 go build . && cp app /usr/local/bin

# step 2
FROM alpine
WORKDIR /app
COPY --from=build_step /usr/local/bin/app .
RUN chmod +x app
EXPOSE 8080/tcp
ENTRYPOINT ./app
# ENTRYPOINT /bin/sh