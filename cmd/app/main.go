package main

import (
	"avitotech/pkg/handler"
	"avitotech/pkg/item"
	"database/sql"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"

	_ "github.com/go-sql-driver/mysql"
)

func main() {
	lg := logrus.New()
	lg.SetFormatter(&logrus.JSONFormatter{})
	lg.SetReportCaller(true)

	dsn := "root:mypassword@tcp(mysql:3306)/avitotech?"
	dsn += "&charset=utf8"
	dsn += "&interpolateParams=true"

	db, err := sql.Open("mysql", dsn)
	if err != nil {
		lg.WithFields(logrus.Fields{
			"error": err,
		}).Error("cannot open mysql")
		return
	}

	db.SetMaxOpenConns(10)

	err = db.Ping()
	if err != nil {
		lg.WithFields(logrus.Fields{
			"error": err,
		}).Error("cannot ping mysql")
		return
	}

	repo := &item.RepoItem{
		DB: db,
	}
	err = repo.Init()
	if err != nil {
		lg.WithFields(logrus.Fields{
			"error": err,
		}).Error("cannot init repo")
		return
	}

	handler := handler.Handler{
		Logger: lg.WithFields(logrus.Fields{
			"handler": "Handler",
		}),
		Repo: repo,
	}

	r := mux.NewRouter()

	r.HandleFunc("/segment", handler.NewSegment).Methods(http.MethodPost)
	r.HandleFunc("/segment", handler.DeleteSegment).Methods(http.MethodDelete)
	r.HandleFunc("/user", handler.AddUser).Methods(http.MethodPost)
	r.HandleFunc("/user", handler.DeleteUser).Methods(http.MethodDelete)
	r.HandleFunc("/{user_id}", handler.UserSegments).Methods(http.MethodGet)

	port := ":8080"
	logrus.WithFields(logrus.Fields{
		"type": "START",
		"port": port,
	}).Info("starting server")

	err = http.ListenAndServe(port, r)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"error": err,
		}).Error("server error")
	}
}
