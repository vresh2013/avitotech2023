# Avitotech2023


## Выполненные задачи
- основная задача (https://github.com/avito-tech/backend-trainee-assignment-2023)


## Как запустить
- скопировать docker-compose.yml
- docker-compose up -d


## Тестирование API утилитой curl
- запусть приложение (см выше)
- см описание curl_test.txt


## API

1. POST /segment {"name":string}
Метод создания сегмента. Принимает название сегмента.
Возвращает код 200 если успешно. Иначе - другой код. Если сегмент уже существует, тоже ошибка

2. DELETE /segment {"name":string}
Метод удаления сегмента. Принимает название сегмента.
Возвращает код 200 если успешно. Иначе - другой код

3. POST /user {"user_id": string, "segments": [string]}
Метод добавления пользователя в сегменты. Принимает id пользователя и список названий сегментов, которые нужно добавить к пользователю. Если сегмента не существует - ошибка. Повторное добавление пользоателя в тот же сегмент игнорируется.
Возвращает код 200 если успешно. Иначе - другой код

4. DELETE /user {"user_id": string, "segments": [string]}
Метод удаления пользователя из сегментов. Принимает id пользователя и список названий сегментов, которые нужно удалить с пользователя. Если пользователя не существует - ошибка. Если не существует такого сегмента или пользователя в нем нет - ошибка. Если пользователь удаляется из всех своих сегментов - пользователь удаляется.
Возвращает код 200 если успешно. Иначе - другой код

5. GET /{user_id}
Метод получения активных сегментов пользователя. Принимает на вход id пользователя.
Возвращает {"user_id": string, "segments": [string]} или ошибку если пользователя нет.
