package handler

import (
	"avitotech/pkg/item"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

// go test -coverprofile='cover.out'
// go tool cover -html='cover.out'

// mockgen -source='../item/interfaces.go' -destination='post_mock.go' -package=handler

func TestGetSegmentName(t *testing.T) {
	name := "time"
	reader := strings.NewReader(fmt.Sprintf("{\"name\":\"%s\"}", name))
	r := httptest.NewRequest("GET", "/", reader)

	ans, err := getSegmentName(r)
	assert.NoError(t, err)
	assert.Equal(t, name, ans)
}

func TestGetSegmentNameNoBody(t *testing.T) {
	r := httptest.NewRequest("GET", "/", nil)
	_, err := getSegmentName(r)
	assert.ErrorIs(t, err, errCannotUnmarshal)
}

func TestNewSegment(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	segmName := "AVITO_VOICE_MESSAGES"

	mockRepo := NewMockRepo(ctrl)
	mockRepo.EXPECT().AddSegment(segmName).Return(nil)
	h := Handler{
		Repo:   mockRepo,
		Logger: logrus.WithFields(logrus.Fields{}),
	}

	w := httptest.NewRecorder()
	reader := strings.NewReader(fmt.Sprintf("{\"name\":\"%s\"}", segmName))
	r := httptest.NewRequest(http.MethodPost, "/segment", reader)

	h.NewSegment(w, r)

	resp := w.Result()
	assert.Equal(t, 200, resp.StatusCode)
}

func TestNewSegmentNoBody(t *testing.T) {
	r := httptest.NewRequest("GET", "/", nil)
	w := httptest.NewRecorder()
	h := Handler{
		Logger: logrus.WithFields(logrus.Fields{}),
	}

	h.NewSegment(w, r)
	result := w.Result()
	assert.Equal(t, 400, result.StatusCode)

	bytes, err := io.ReadAll(result.Body)
	assert.NoError(t, err)
	mess := string(bytes)
	assert.Equal(t, "no segment name\n", mess)
}

func TestNewSegmentInternalErr(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := NewMockRepo(ctrl)
	h := Handler{
		Logger: logrus.WithFields(logrus.Fields{}),
		Repo:   mockRepo,
	}

	name := "name123"
	mockRepo.EXPECT().AddSegment(name).Return(errors.New("some error"))

	w := httptest.NewRecorder()
	reader := strings.NewReader(fmt.Sprintf("{\"name\":\"%s\"}", name))
	r := httptest.NewRequest(http.MethodPost, "/segment", reader)
	h.NewSegment(w, r)

	result := w.Result()
	assert.Equal(t, 500, result.StatusCode)
	bytes, err := io.ReadAll(result.Body)
	assert.NoError(t, err)
	assert.Equal(t, "Internal server error\n", string(bytes))
}

func TestNewSegmentErrSegmentExists(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := NewMockRepo(ctrl)
	h := Handler{
		Logger: logrus.WithFields(logrus.Fields{}),
		Repo:   mockRepo,
	}

	name := "name123"
	mockRepo.EXPECT().AddSegment(name).Return(item.ErrSegmExists)

	w := httptest.NewRecorder()
	reader := strings.NewReader(fmt.Sprintf("{\"name\":\"%s\"}", name))
	r := httptest.NewRequest(http.MethodPost, "/segment", reader)
	h.NewSegment(w, r)

	result := w.Result()
	assert.Equal(t, 400, result.StatusCode)
	bytes, err := io.ReadAll(result.Body)
	assert.NoError(t, err)
	assert.Equal(t, "segment exists\n", string(bytes))
}

func TestDeleteSegment(t *testing.T) {
	name := "name123"
	reader := strings.NewReader(fmt.Sprintf("{\"name\":\"%s\"}", name))
	r := httptest.NewRequest(http.MethodDelete, "/segment", reader)
	w := httptest.NewRecorder()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := NewMockRepo(ctrl)
	h := Handler{
		Logger: logrus.WithFields(logrus.Fields{}),
		Repo:   mockRepo,
	}

	mockRepo.EXPECT().DeleteSegment(name).Return(nil)

	h.DeleteSegment(w, r)

	result := w.Result()
	assert.Equal(t, 200, result.StatusCode)
}

func TestDeleteSegmentNoSegmName(t *testing.T) {
	r := httptest.NewRequest(http.MethodDelete, "/segment", nil)
	w := httptest.NewRecorder()

	h := Handler{
		Logger: logrus.WithFields(logrus.Fields{}),
	}

	h.DeleteSegment(w, r)

	result := w.Result()
	assert.Equal(t, 400, result.StatusCode)
	bytes, err := io.ReadAll(result.Body)
	assert.NoError(t, err)
	assert.Equal(t, "no segment name\n", string(bytes))
}

func TestDeleteSegmentInternalErr(t *testing.T) {
	name := "name123"
	reader := strings.NewReader(fmt.Sprintf("{\"name\":\"%s\"}", name))
	r := httptest.NewRequest(http.MethodDelete, "/segment", reader)
	w := httptest.NewRecorder()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockRepo := NewMockRepo(ctrl)
	mockRepo.EXPECT().DeleteSegment(name).Return(errors.New("some error"))

	h := Handler{
		Logger: logrus.WithFields(logrus.Fields{}),
		Repo:   mockRepo,
	}
	h.DeleteSegment(w, r)

	result := w.Result()
	assert.Equal(t, 500, result.StatusCode)
	bytes, err := io.ReadAll(result.Body)
	assert.NoError(t, err)
	assert.Equal(t, "Internal server error\n", string(bytes))
}

func TestDeleteSegmentErrNoSegment(t *testing.T) {
	name := "name123"
	reader := strings.NewReader(fmt.Sprintf("{\"name\":\"%s\"}", name))
	r := httptest.NewRequest(http.MethodDelete, "/segment", reader)
	w := httptest.NewRecorder()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockRepo := NewMockRepo(ctrl)
	mockRepo.EXPECT().DeleteSegment(name).Return(item.ErrNoSegment)

	h := Handler{
		Logger: logrus.WithFields(logrus.Fields{}),
		Repo:   mockRepo,
	}
	h.DeleteSegment(w, r)

	result := w.Result()
	assert.Equal(t, 400, result.StatusCode)
	bytes, err := io.ReadAll(result.Body)
	assert.NoError(t, err)
	assert.Equal(t, "No such segment\n", string(bytes))
}

func TestGetUserIDSegments(t *testing.T) {
	userAndSegms := userSegments{
		UserID:   "123",
		Segments: []string{"segment1, segemnt2"},
	}
	js, err := json.Marshal(userAndSegms)
	assert.NoError(t, err)
	reader := bytes.NewReader(js)

	r := httptest.NewRequest(http.MethodGet, "/", reader)
	ans, err := getUserIDSegments(r)
	assert.NoError(t, err)
	assert.Equal(t, userAndSegms, ans)
}

func TestGetUserIDSegmentsNoBody(t *testing.T) {
	r := httptest.NewRequest(http.MethodGet, "/", nil)
	_, err := getUserIDSegments(r)
	assert.ErrorIs(t, err, errCannotUnmarshal)
}

func TestAddUser(t *testing.T) {
	userSgms := userSegments{
		UserID:   "1234",
		Segments: []string{"1", "2"},
	}
	b, err := json.Marshal(userSgms)
	assert.NoError(t, err)
	reader := bytes.NewReader(b)

	r := httptest.NewRequest(http.MethodPost, "/user", reader)
	w := httptest.NewRecorder()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockRepo := NewMockRepo(ctrl)

	h := Handler{
		Logger: logrus.WithFields(logrus.Fields{}),
		Repo:   mockRepo,
	}
	mockRepo.EXPECT().AddUserSegments(userSgms.UserID, userSgms.Segments).Return(nil)

	h.AddUser(w, r)

	result := w.Result()
	assert.Equal(t, 200, result.StatusCode)
}

func TestAddUserNoBody(t *testing.T) {
	r := httptest.NewRequest(http.MethodPost, "/user", nil)
	w := httptest.NewRecorder()

	h := Handler{
		Logger: logrus.WithFields(logrus.Fields{}),
	}
	h.AddUser(w, r)

	result := w.Result()
	assert.Equal(t, 400, result.StatusCode)
	b, err := io.ReadAll(result.Body)
	assert.NoError(t, err)

	assert.Equal(t, "cannot get user_id and segments\n", string(b))
}

func TestAddUserInternalErr(t *testing.T) {
	userSgms := userSegments{
		UserID:   "1234",
		Segments: []string{"1", "2"},
	}
	b, err := json.Marshal(userSgms)
	assert.NoError(t, err)
	reader := bytes.NewReader(b)

	r := httptest.NewRequest(http.MethodPost, "/user", reader)
	w := httptest.NewRecorder()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockRepo := NewMockRepo(ctrl)

	h := Handler{
		Logger: logrus.WithFields(logrus.Fields{}),
		Repo:   mockRepo,
	}

	mockRepo.EXPECT().AddUserSegments(userSgms.UserID, userSgms.Segments).Return(errors.New("some error"))

	h.AddUser(w, r)

	result := w.Result()
	assert.Equal(t, 500, result.StatusCode)

	ansBytes, err := io.ReadAll(result.Body)
	assert.NoError(t, err)
	assert.Equal(t, "Internal server error\n", string(ansBytes))
}

func TestAddUserErrNoSegment(t *testing.T) {
	userSgms := userSegments{
		UserID:   "1234",
		Segments: []string{"1", "2"},
	}
	b, err := json.Marshal(userSgms)
	assert.NoError(t, err)
	reader := bytes.NewReader(b)

	r := httptest.NewRequest(http.MethodPost, "/user", reader)
	w := httptest.NewRecorder()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockRepo := NewMockRepo(ctrl)

	h := Handler{
		Logger: logrus.WithFields(logrus.Fields{}),
		Repo:   mockRepo,
	}

	mockRepo.EXPECT().AddUserSegments(userSgms.UserID, userSgms.Segments).Return(item.ErrNoSegment)

	h.AddUser(w, r)

	result := w.Result()
	assert.Equal(t, 400, result.StatusCode)

	ansBytes, err := io.ReadAll(result.Body)
	assert.NoError(t, err)
	assert.Equal(t, "no such segment\n", string(ansBytes))
}

func TestDeleteUser(t *testing.T) {
	userSgms := userSegments{
		UserID:   "123",
		Segments: []string{"segment"},
	}
	b, err := json.Marshal(userSgms)
	assert.NoError(t, err)
	reader := bytes.NewReader(b)

	r := httptest.NewRequest(http.MethodDelete, "/user", reader)
	w := httptest.NewRecorder()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockRepo := NewMockRepo(ctrl)
	h := Handler{
		Logger: logrus.WithFields(logrus.Fields{}),
		Repo:   mockRepo,
	}

	mockRepo.EXPECT().DeleteUserSegments(userSgms.UserID, userSgms.Segments).Return(nil)

	h.DeleteUser(w, r)

	result := w.Result()
	assert.Equal(t, 200, result.StatusCode)
}

func TestDeleteUserNoBody(t *testing.T) {
	r := httptest.NewRequest(http.MethodDelete, "/user", nil)
	w := httptest.NewRecorder()

	h := Handler{
		Logger: logrus.WithFields(logrus.Fields{}),
	}

	h.DeleteUser(w, r)

	result := w.Result()
	assert.Equal(t, 400, result.StatusCode)

	b, err := io.ReadAll(result.Body)
	assert.NoError(t, err)
	assert.Equal(t, "cannot get user_id and segments\n", string(b))
}

func TestDeleteUserInternalErr(t *testing.T) {
	userSgms := userSegments{
		UserID:   "123",
		Segments: []string{"segment"},
	}
	b, err := json.Marshal(userSgms)
	assert.NoError(t, err)
	reader := bytes.NewReader(b)

	r := httptest.NewRequest(http.MethodDelete, "/user", reader)
	w := httptest.NewRecorder()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockRepo := NewMockRepo(ctrl)
	h := Handler{
		Logger: logrus.WithFields(logrus.Fields{}),
		Repo:   mockRepo,
	}

	mockRepo.EXPECT().DeleteUserSegments(userSgms.UserID, userSgms.Segments).Return(errors.New("some error"))

	h.DeleteUser(w, r)

	result := w.Result()
	assert.Equal(t, 500, result.StatusCode)

	ansBytes, err := io.ReadAll(result.Body)
	assert.NoError(t, err)
	assert.Equal(t, "Internal server error\n", string(ansBytes))
}

func TestDeleteUserErrNoDeletion(t *testing.T) {
	userSgms := userSegments{
		UserID:   "123",
		Segments: []string{"segment"},
	}
	b, err := json.Marshal(userSgms)
	assert.NoError(t, err)
	reader := bytes.NewReader(b)

	r := httptest.NewRequest(http.MethodDelete, "/user", reader)
	w := httptest.NewRecorder()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockRepo := NewMockRepo(ctrl)
	h := Handler{
		Logger: logrus.WithFields(logrus.Fields{}),
		Repo:   mockRepo,
	}

	mockRepo.EXPECT().DeleteUserSegments(userSgms.UserID, userSgms.Segments).Return(item.ErrNoDeletion)

	h.DeleteUser(w, r)

	result := w.Result()
	assert.Equal(t, 400, result.StatusCode)

	ansBytes, err := io.ReadAll(result.Body)
	assert.NoError(t, err)
	assert.Equal(t, "nothing to delete\n", string(ansBytes))
}

func TestDeleteUserErrNoSegment(t *testing.T) {
	userSgms := userSegments{
		UserID:   "123",
		Segments: []string{"segment"},
	}
	b, err := json.Marshal(userSgms)
	assert.NoError(t, err)
	reader := bytes.NewReader(b)

	r := httptest.NewRequest(http.MethodDelete, "/user", reader)
	w := httptest.NewRecorder()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockRepo := NewMockRepo(ctrl)
	h := Handler{
		Logger: logrus.WithFields(logrus.Fields{}),
		Repo:   mockRepo,
	}

	mockRepo.EXPECT().DeleteUserSegments(userSgms.UserID, userSgms.Segments).Return(item.ErrNoSegment)

	h.DeleteUser(w, r)

	result := w.Result()
	assert.Equal(t, 400, result.StatusCode)

	ansBytes, err := io.ReadAll(result.Body)
	assert.NoError(t, err)
	assert.Equal(t, "no such segment\n", string(ansBytes))
}

func TestUserSegments(t *testing.T) {
	user := userSegments{
		UserID:   "123",
		Segments: []string{"1", "2"},
	}
	r := httptest.NewRequest(http.MethodGet, fmt.Sprintf("/%s", user.UserID), nil)
	w := httptest.NewRecorder()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockRepo := NewMockRepo(ctrl)

	h := Handler{
		Logger: logrus.WithFields(logrus.Fields{}),
		Repo:   mockRepo,
	}

	mockRepo.EXPECT().UserSegments(user.UserID).Return(user.Segments, nil)

	router := mux.NewRouter()
	router.HandleFunc("/{user_id}", h.UserSegments)
	router.ServeHTTP(w, r)

	result := w.Result()
	assert.Equal(t, 200, result.StatusCode)

	b, err := io.ReadAll(result.Body)
	assert.NoError(t, err)
	ansUser := userSegments{}
	err = json.Unmarshal(b, &ansUser)
	assert.NoError(t, err)

	assert.Equal(t, user, ansUser)
}

func TestUserSegmentsNoUserID(t *testing.T) {
	r := httptest.NewRequest(http.MethodGet, "/", nil)
	w := httptest.NewRecorder()

	h := Handler{
		Logger: logrus.WithFields(logrus.Fields{}),
	}

	h.UserSegments(w, r)

	result := w.Result()
	assert.Equal(t, 500, result.StatusCode)

	b, err := io.ReadAll(result.Body)
	assert.NoError(t, err)

	assert.Equal(t, "Internal server error\n", string(b))
}

func TestUserSegmentsInternalErr(t *testing.T) {
	user := userSegments{
		UserID:   "123",
		Segments: []string{"1", "2"},
	}
	r := httptest.NewRequest(http.MethodGet, fmt.Sprintf("/%s", user.UserID), nil)
	w := httptest.NewRecorder()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockRepo := NewMockRepo(ctrl)

	h := Handler{
		Logger: logrus.WithFields(logrus.Fields{}),
		Repo:   mockRepo,
	}

	mockRepo.EXPECT().UserSegments(user.UserID).Return(nil, errors.New("some error"))

	router := mux.NewRouter()
	router.HandleFunc("/{user_id}", h.UserSegments)
	router.ServeHTTP(w, r)

	result := w.Result()
	assert.Equal(t, 500, result.StatusCode)

	b, err := io.ReadAll(result.Body)
	assert.NoError(t, err)
	assert.Equal(t, "Internal server error\n", string(b))
}

func TestUserSegmentErrNoUser(t *testing.T) {
	user := userSegments{
		UserID:   "123",
		Segments: []string{"1", "2"},
	}
	r := httptest.NewRequest(http.MethodGet, fmt.Sprintf("/%s", user.UserID), nil)
	w := httptest.NewRecorder()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockRepo := NewMockRepo(ctrl)

	h := Handler{
		Logger: logrus.WithFields(logrus.Fields{}),
		Repo:   mockRepo,
	}

	mockRepo.EXPECT().UserSegments(user.UserID).Return(nil, item.ErrNoUser)

	router := mux.NewRouter()
	router.HandleFunc("/{user_id}", h.UserSegments)
	router.ServeHTTP(w, r)

	result := w.Result()
	assert.Equal(t, 400, result.StatusCode)

	b, err := io.ReadAll(result.Body)
	assert.NoError(t, err)
	assert.Equal(t, "No such user\n", string(b))
}
