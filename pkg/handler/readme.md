Посмотреть coverprofile:
- go tool cover -html='cover.out'

При изменении тестов нужно обновить cover.out;
- go test -coverprofile='cover.out'

Мок создан по команде:
- mockgen -source='../item/interfaces.go' -destination='post_mock.go' -package=handler