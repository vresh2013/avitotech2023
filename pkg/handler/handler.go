package handler

import (
	"avitotech/pkg/item"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

var (
	errCannotReadBody  = errors.New("cannot read body")
	errCannotUnmarshal = errors.New("cannot unmarshal")
)

type Handler struct {
	Repo   item.Repo
	Logger *logrus.Entry
}

func (h *Handler) NewSegment(w http.ResponseWriter, r *http.Request) {
	segmentName, err := getSegmentName(r)
	if err != nil {
		h.Logger.WithFields(logrus.Fields{
			"error": err,
		}).Error("cannot get segment name")
		http.Error(w, "no segment name", 400)
		return
	}
	defer r.Body.Close()

	err = h.Repo.AddSegment(segmentName)
	if err != nil {
		if errors.Is(err, item.ErrSegmExists) {
			http.Error(w, "segment exists", 400)
			return
		}
		h.Logger.WithFields(logrus.Fields{
			"error": err,
		}).Error("cannot add segment")
		http.Error(w, "Internal server error", 500)
		return
	}

	w.WriteHeader(200)
}

func (h *Handler) DeleteSegment(w http.ResponseWriter, r *http.Request) {
	sgName, err := getSegmentName(r)
	if err != nil {
		h.Logger.WithFields(logrus.Fields{
			"error": err,
		}).Error("cannot get segment name")
		http.Error(w, "no segment name", 400)
		return
	}
	defer r.Body.Close()

	err = h.Repo.DeleteSegment(sgName)
	if err != nil {
		if errors.Is(err, item.ErrNoSegment) {
			http.Error(w, "No such segment", 400)
			return
		}

		h.Logger.WithFields(logrus.Fields{
			"error": err,
		}).Error("cannot delete segment")
		http.Error(w, "Internal server error", 500)
		return
	}

	w.WriteHeader(200)
}

func (h *Handler) AddUser(w http.ResponseWriter, r *http.Request) {
	userSgms, err := getUserIDSegments(r)
	if err != nil {
		h.Logger.WithFields(logrus.Fields{
			"error": err,
		}).Error("cannot get userID and segments")
		http.Error(w, "cannot get user_id and segments", 400)
		return
	}
	defer r.Body.Close()

	err = h.Repo.AddUserSegments(userSgms.UserID, userSgms.Segments)
	if err != nil {
		if errors.Is(err, item.ErrNoSegment) {
			http.Error(w, "no such segment", 400)
			return
		}
		h.Logger.WithFields(logrus.Fields{
			"error": err,
		}).Error("cannot add user with segments")
		http.Error(w, "Internal server error", 500)
		return
	}

	w.WriteHeader(200)
}

func (h *Handler) DeleteUser(w http.ResponseWriter, r *http.Request) {
	userSgms, err := getUserIDSegments(r)
	if err != nil {
		h.Logger.WithFields(logrus.Fields{
			"error": err,
		}).Error("cannot get userID and segments")
		http.Error(w, "cannot get user_id and segments", 400)
		return
	}
	defer r.Body.Close()

	err = h.Repo.DeleteUserSegments(userSgms.UserID, userSgms.Segments)
	if err != nil {
		if errors.Is(err, item.ErrNoDeletion) {
			http.Error(w, "nothing to delete", 400)
			return
		}
		if errors.Is(err, item.ErrNoSegment) {
			http.Error(w, "no such segment", 400)
			return
		}
		h.Logger.WithFields(logrus.Fields{
			"error": err,
		}).Error("cannot delete by userID and segments")
		http.Error(w, "Internal server error", 500)
		return
	}

	w.WriteHeader(200)
}

func (h *Handler) UserSegments(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userID, ok := vars["user_id"]
	if !ok {
		h.Logger.Error("cannot get user_id")
		http.Error(w, "Internal server error", 500)
		return
	}

	segments, err := h.Repo.UserSegments(userID)
	if err != nil {
		if errors.Is(item.ErrNoUser, err) {
			http.Error(w, "No such user", 400)
			return
		}
		h.Logger.WithFields(logrus.Fields{
			"error": err,
		}).Error("get segments by id")
		http.Error(w, "Internal server error", 500)
		return
	}

	ans := userSegments{
		UserID:   userID,
		Segments: segments,
	}

	js, err := json.Marshal(ans)
	if err != nil {
		h.Logger.WithFields(logrus.Fields{
			"error": err,
		}).Error("cannot convert to json")
		http.Error(w, "Internal server error", 500)
		return
	}

	_, err = w.Write(js)
	if err != nil {
		h.Logger.WithFields(logrus.Fields{
			"error": err,
		}).Error("cannot write json")
		http.Error(w, "Internal server error", 500)
		return
	}

	w.WriteHeader(200)
}

type segment struct {
	Name string `json:"name"`
}

func getSegmentName(r *http.Request) (string, error) {
	data, err := io.ReadAll(r.Body)
	if err != nil {
		return "", fmt.Errorf("%w: %s", errCannotReadBody, err.Error())
	}

	// fmt.Println("body =", string(data))

	segment := segment{}
	err = json.Unmarshal(data, &segment)
	if err != nil {
		return "", fmt.Errorf("%w:, %s", errCannotUnmarshal, err.Error())
	}

	return segment.Name, nil
}

type userSegments struct {
	UserID   string   `json:"user_id"`
	Segments []string `json:"segments"`
}

func getUserIDSegments(r *http.Request) (userSegments, error) {
	body, err := io.ReadAll(r.Body)

	// fmt.Println("body =", string(body))

	if err != nil {
		return userSegments{}, fmt.Errorf("%w: %s", errCannotReadBody, err.Error())
	}

	usrSgms := userSegments{}
	err = json.Unmarshal(body, &usrSgms)
	if err != nil {
		return userSegments{}, fmt.Errorf("%w:, %s", errCannotUnmarshal, err.Error())
	}

	return usrSgms, nil
}
