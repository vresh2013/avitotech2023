package item

// mockgen -source='interfaces.go' -destination='../handler/post_mock.go' -package=handler
type Repo interface {
	AddSegment(string) error
	DeleteSegment(string) error
	AddUserSegments(userID string, segments []string) error
	DeleteUserSegments(userID string, segments []string) error
	UserSegments(userID string) ([]string, error)
}

type Dummy interface {
	Foo() bool
}
