package item

import (
	"errors"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"
)

func TestAddSegment(t *testing.T) {
	r := &RepoItem{
		segments: make([]string, 0),
	}
	newSegm := "new_segment"
	err := r.AddSegment(newSegm)
	assert.Equal(t, nil, err)
	assert.Equal(t, []string{newSegm}, r.segments)
}

func TestAddSegmentDublicate(t *testing.T) {
	segm := "segment"
	r := &RepoItem{
		segments: []string{segm},
	}
	err := r.AddSegment(segm)
	assert.Equal(t, ErrSegmExists, err)
	assert.Equal(t, []string{segm}, r.segments)
}

func TestDeleteSegment(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)
	defer db.Close()

	segm := "segment"
	r := &RepoItem{
		segments: []string{segm},
		DB:       db,
	}

	mock.ExpectExec("DELETE FROM user_segment WHERE segment =").WithArgs(segm).WillReturnResult(sqlmock.NewResult(1, 1)).WillReturnError(nil)

	err = r.DeleteSegment(segm)
	assert.Equal(t, nil, err)
}

func TestDeleteSegmentNoSegment(t *testing.T) {
	segm := "segment"
	r := &RepoItem{
		segments: []string{"other segment"},
	}
	err := r.DeleteSegment(segm)
	assert.Equal(t, err, ErrNoSegment)
}

func TestDeleteSegmentErrDB(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)
	defer db.Close()

	segm := "segment"
	r := &RepoItem{
		segments: []string{segm},
		DB:       db,
	}

	someErr := errors.New("some error")
	mock.ExpectExec("DELETE FROM user_segment WHERE segment =").WithArgs(segm).WillReturnResult(sqlmock.NewResult(1, 1)).WillReturnError(someErr)

	err = r.DeleteSegment(segm)
	assert.Equal(t, err, someErr)
}

func TestAddUserSegmentsAllNewSegments(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)
	defer db.Close()

	segments := []string{"segment1", "segment2"}
	userID := "user_id"
	r := &RepoItem{
		segments: segments,
		DB:       db,
	}

	mock.ExpectQuery("SELECT segment FROM user_segment WHERE user_id =").WithArgs(userID).
		WillReturnRows(sqlmock.NewRows([]string{"segment"})).WillReturnError(nil)

	for _, sg := range segments {
		mock.ExpectExec("INSERT INTO user_segment").WithArgs(userID, sg).
			WillReturnResult(sqlmock.NewResult(1, 1)).WillReturnError(nil)
	}

	err = r.AddUserSegments(userID, segments)
	assert.NoError(t, err)
}

func TestAddUserSegmentsNoNewRows(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)
	defer db.Close()

	segments := []string{"segment1", "segment2"}
	userID := "user_id"
	r := &RepoItem{
		segments: segments,
		DB:       db,
	}

	rows := sqlmock.NewRows([]string{"segment"})
	for _, sg := range segments {
		rows.AddRow(sg)
	}

	mock.ExpectQuery("SELECT segment FROM user_segment WHERE user_id =").WithArgs(userID).
		WillReturnRows(rows).WillReturnError(nil)

	for _, sg := range segments {
		mock.ExpectExec("INSERT INTO user_segment").WithArgs(userID, sg).
			WillReturnResult(sqlmock.NewResult(1, 1)).WillReturnError(nil)
	}

	err = r.AddUserSegments(userID, segments)
	assert.NoError(t, err)
}

// почему-то не работает
// func TestTestAddUserSegmentsRowsErr(t *testing.T) {
// 	db, mock, err := sqlmock.New()
// 	assert.NoError(t, err)
// 	defer db.Close()

// 	segments := []string{"segment1", "segment2"}
// 	userID := "user_id"
// 	r := &RepoItem{
// 		segments: segments,
// 		DB:       db,
// 	}

// 	rowErr := errors.New("some error")
// 	rows := sqlmock.NewRows([]string{"segment"}).AddRow("segment3").RowError(0, rowErr)

// 	mock.ExpectQuery("SELECT segment FROM user_segment WHERE user_id =").WithArgs(userID).
// 		WillReturnRows(rows)

// 	err = r.AddUserSegments(userID, segments)
// 	assert.Equal(t, rowErr, err)
// }

func TestAddUserSegmentsNoSegment(t *testing.T) {
	sgExist := "exists"
	sgNotExist := "not exists"
	r := &RepoItem{
		segments: []string{sgExist},
	}

	err := r.AddUserSegments("user_id", []string{sgExist, sgNotExist})
	assert.Equal(t, err, ErrNoSegment)
}

func TestAddUserSegmentsQueryErr(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)
	defer db.Close()

	segments := []string{"segment1", "segment2"}
	userID := "user_id"
	r := &RepoItem{
		segments: segments,
		DB:       db,
	}

	someErr := errors.New("some error")
	mock.ExpectQuery("SELECT segment FROM user_segment WHERE user_id =").WithArgs(userID).
		WillReturnError(someErr)

	err = r.AddUserSegments(userID, segments)
	assert.Equal(t, err, someErr)
}

func TestAddUserSegmentsErrExec(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)
	defer db.Close()

	newSegments := []string{"segment1"}
	userID := "user_id"
	r := &RepoItem{
		segments: newSegments,
		DB:       db,
	}

	mock.ExpectQuery("SELECT segment FROM user_segment WHERE user_id =").WithArgs(userID).
		WillReturnRows(sqlmock.NewRows([]string{"segment"}))

	someErr := errors.New("some error")
	mock.ExpectExec("INSERT INTO user_segment").WithArgs(userID, newSegments[0]).
		WillReturnResult(sqlmock.NewResult(1, 1)).WillReturnError(someErr)

	err = r.AddUserSegments(userID, newSegments)
	assert.Equal(t, someErr, err)
}

func TestDeleteUserSegments(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)
	defer db.Close()

	segments := []string{"segment1", "segment2"}
	r := &RepoItem{
		segments: segments,
		DB:       db,
	}

	userID := "123"
	for _, sg := range segments {
		mock.ExpectExec("DELETE FROM user_segment WHERE").WithArgs(userID, sg).
			WillReturnResult(sqlmock.NewResult(1, 1))
	}

	err = r.DeleteUserSegments(userID, segments)
	assert.NoError(t, err)
}

func TestDeleteUserSegmentsNoSegment(t *testing.T) {
	r := &RepoItem{
		segments: make([]string, 0),
	}
	err := r.DeleteUserSegments("userID", []string{"not existing segment"})
	assert.Equal(t, ErrNoSegment, err)
}

func TestDeleteUserExecErr(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)
	defer db.Close()

	segments := []string{"segment1"}
	r := &RepoItem{
		segments: segments,
		DB:       db,
	}

	userID := "123"
	someErr := errors.New("some error")
	mock.ExpectExec("DELETE FROM user_segment WHERE").WithArgs(userID, segments[0]).
		WillReturnError(someErr)

	err = r.DeleteUserSegments(userID, segments)
	assert.Equal(t, someErr, err)
}

func TestDeleteUserNoDeletion(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)
	defer db.Close()

	segments := []string{"segment1"}
	r := &RepoItem{
		segments: segments,
		DB:       db,
	}

	userID := "123"
	mock.ExpectExec("DELETE FROM user_segment WHERE").WithArgs(userID, segments[0]).
		WillReturnResult(sqlmock.NewResult(0, 0))

	err = r.DeleteUserSegments(userID, segments)
	assert.Equal(t, ErrNoDeletion, err)
}

func TestUserSegment(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)
	defer db.Close()

	segments := []string{"segment1", "segment2"}

	r := &RepoItem{
		DB:       db,
		segments: segments,
	}

	userID := "userID"
	rows := sqlmock.NewRows([]string{"segment"}).AddRow(segments[0]).AddRow(segments[1])
	mock.ExpectQuery("SELECT segment FROM user_segment WHERE").WithArgs(userID).WillReturnRows(rows)

	// счетчик rows перематывается, поэтому надо переопределить
	rows = sqlmock.NewRows([]string{"segment"}).AddRow(segments[0]).AddRow(segments[1])
	mock.ExpectQuery("SELECT segment FROM user_segment WHERE").WithArgs(userID).WillReturnRows(rows)

	userSgms, err := r.UserSegments(userID)
	assert.NoError(t, err)
	assert.Equal(t, segments, userSgms)
}

func TestUserSegmentErrNoUser(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)
	defer db.Close()

	segments := []string{"segment1"}

	r := &RepoItem{
		DB:       db,
		segments: segments,
	}

	userID := "userID"
	mock.ExpectQuery("SELECT segment FROM user_segment WHERE").WithArgs(userID).
		WillReturnRows(sqlmock.NewRows([]string{"segment"}))
	_, err = r.UserSegments(userID)
	assert.Equal(t, ErrNoUser, err)
}

func TestUserSegmentQueryErr(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)
	defer db.Close()

	segments := []string{"segment1"}

	r := &RepoItem{
		DB:       db,
		segments: segments,
	}

	userID := "userID"
	someErr := errors.New("some error")
	mock.ExpectQuery("SELECT segment FROM user_segment WHERE").WithArgs(userID).WillReturnError(someErr)
	_, err = r.UserSegments(userID)
	assert.Equal(t, someErr, err)
}

func TestUserSegmentQueryErr2(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)
	defer db.Close()

	segments := []string{"segment1"}

	r := &RepoItem{
		DB:       db,
		segments: segments,
	}
	userID := "userID"
	row := sqlmock.NewRows([]string{"segment"}).AddRow("segment1")
	mock.ExpectQuery("SELECT segment FROM user_segment WHERE").WithArgs(userID).WillReturnRows(row)

	someErr := errors.New("some error")
	mock.ExpectQuery("SELECT segment FROM user_segment WHERE").WithArgs(userID).WillReturnError(someErr)
	_, err = r.UserSegments(userID)
	assert.Equal(t, someErr, err)
}

func TestAreSegmentsExists(t *testing.T) {
	segments := []string{"segment1"}
	r := &RepoItem{
		segments: []string{segments[0], "segment2"},
	}
	assert.Equal(t, true, r.areSegmentsExists(segments))

	type testStruct struct {
		repoSegments  []string
		inputSegments []string
		answer        bool
	}

	tests := []testStruct{
		{
			repoSegments:  []string{"segment1", "segment2"},
			inputSegments: []string{"segment1"},
			answer:        true,
		},
		{
			repoSegments:  []string{"segment1"},
			inputSegments: []string{"segment1", "segment2"},
			answer:        false,
		},
		{
			repoSegments:  make([]string, 0),
			inputSegments: []string{"segment"},
			answer:        false,
		},
		{
			repoSegments:  []string{"segment"},
			inputSegments: []string{"segment"},
			answer:        true,
		},
	}

	for _, test := range tests {
		r := &RepoItem{
			segments: test.repoSegments,
		}
		assert.Equal(t, test.answer, r.areSegmentsExists(test.inputSegments),
			fmt.Sprintf("repoSegments = %v, inputSegments = %v, answer = %v",
				test.repoSegments, test.inputSegments, test.answer))
	}
}
