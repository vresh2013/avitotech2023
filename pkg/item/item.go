package item

import (
	"database/sql"
	"errors"

	_ "github.com/go-sql-driver/mysql"
)

var (
	ErrSegmExists = errors.New("segment exists")
	ErrNoSegment  = errors.New("no such segment")
	ErrNoDeletion = errors.New("nothing to delete")
	ErrNoUser     = errors.New("no such user")
)

type RepoItem struct {
	DB       *sql.DB
	segments []string
}

func (r *RepoItem) Init() error {
	r.segments = make([]string, 0)

	rows, err := r.DB.Query("SELECT DISTINCT segment FROM user_segment")
	if err != nil {
		return err
	}

	for rows.Next() {
		sg := ""
		err = rows.Scan(&sg)
		if err != nil {
			return err
		}
		r.segments = append(r.segments, sg)
	}
	return nil
}

func (r *RepoItem) AddSegment(segm string) error {
	isFound := false
	for _, s := range r.segments {
		if s == segm {
			isFound = true
			break
		}
	}

	if isFound {
		return ErrSegmExists
	}

	r.segments = append(r.segments, segm)
	return nil
}

func (r *RepoItem) DeleteSegment(segm string) error {
	ind := -1
	for i, s := range r.segments {
		if segm == s {
			ind = i
			break
		}
	}

	if ind == -1 {
		return ErrNoSegment
	}

	// вроде работат при len = 2, 1
	r.segments = append(r.segments[:ind], r.segments[ind+1:]...)

	_, err := r.DB.Exec("DELETE FROM user_segment WHERE segment = ?", segm)
	if err != nil {
		return err
	}

	return nil
}

func (r *RepoItem) AddUserSegments(userID string, segments []string) error {
	if !r.areSegmentsExists(segments) {
		return ErrNoSegment
	}

	rows, err := r.DB.Query("SELECT segment FROM user_segment WHERE user_id = ?", userID)
	if err != nil {
		return err
	}
	oldSegments := make([]string, 0)
	for rows.Next() {
		segm := ""
		err = rows.Scan(&segm)
		if err != nil {
			return err
		}
		oldSegments = append(oldSegments, segm)
	}

	for _, sg := range segments {
		if match(sg, oldSegments) {
			continue
		}

		_, err := r.DB.Exec("INSERT INTO user_segment (`user_id`, `segment`) VALUES (?, ?)", userID, sg)
		if err != nil {
			return err
		}
	}

	return nil
}

func (r *RepoItem) DeleteUserSegments(userID string, segments []string) error {
	if !r.areSegmentsExists(segments) {
		return ErrNoSegment
	}

	for _, sg := range segments {
		result, err := r.DB.Exec("DELETE FROM user_segment WHERE user_id = ? AND segment = ?", userID, sg)
		if err != nil {
			return err
		}

		affected, err := result.RowsAffected()
		if err != nil {
			return err
		}

		if affected == 0 {
			return ErrNoDeletion
		}
	}

	return nil
}

func (r *RepoItem) UserSegments(userID string) ([]string, error) {
	row := r.DB.QueryRow("SELECT segment FROM user_segment WHERE user_id = ?", userID)
	if row.Err() != nil {
		return nil, row.Err()
	}
	dummy := ""
	err := row.Scan(&dummy)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, ErrNoUser
		}
		return nil, err
	}

	rows, err := r.DB.Query("SELECT segment FROM user_segment WHERE user_id = ?", userID)
	if err != nil {
		// query не возвращает sql.ErrNoRows
		// if errors.Is(sql.ErrNoRows, err) {
		// 	return nil, ErrNoUser
		// }
		return nil, err
	}

	segments := make([]string, 0)
	for rows.Next() {
		sg := ""
		err = rows.Scan(&sg)
		if err != nil {
			return nil, err
		}
		segments = append(segments, sg)
	}

	return segments, nil
}

func (r *RepoItem) areSegmentsExists(segments []string) bool {
	for _, sgm := range segments {
		isFound := false
		for _, repoSgm := range r.segments {
			if sgm == repoSgm {
				isFound = true
				break
			}
		}
		if !isFound {
			return false
		}
	}
	return true
}

func match(target string, sl []string) bool {
	for _, s := range sl {
		if target == s {
			return true
		}
	}
	return false
}
